const i = document.querySelectorAll(".fas");
i.forEach((elem) => {
  const input = elem.previousElementSibling;
  elem.addEventListener("click", function () {
    if (input.getAttribute("type", "password")) {
      input.setAttribute("type", "text");
      elem.classList.replace("fa-eye", "fa-eye-slash");
    } else if (input.getAttribute("type", "text")) {
      elem.classList.replace("fa-eye-slash", "fa-eye");
    }
  });
});

document.getElementById("confirm").addEventListener("click", function () {
  const inputPass = document.getElementById("password").value;
  const inputConfirm = document.getElementById("password-confirm").value;
  if (`${inputPass}` === `${inputConfirm}`) {
     hideError();
    alert("You are welcome!")
            
  } else 
    if (`${ inputPass}` !== `${inputConfirm}` ){
     showError()
    }
  }
);

function showError() {
    const errorElem = document.querySelector('.error');
    errorElem.classList.add('visible')
}
function hideError() {
    const errorElem = document.querySelector('.error');
    errorElem.classList.remove('visible')
}